import styles from './Navbar.module.css';
import {NavLink} from "react-router-dom";

function Navbar() {
    return(
        <nav className={styles.nav}>
            <NavLink to="/profile"><div>Profile</div></NavLink>
            <NavLink to="/messages"><div>Messages</div></NavLink>
            <NavLink to="/friends"><div>Friends</div></NavLink>
            <NavLink to="/music"><div>Music</div></NavLink>
            <NavLink to="/"><div>News</div></NavLink>
            <NavLink to="/settings"><div>Settings</div></NavLink>
        </nav>
    );
}

export default Navbar;