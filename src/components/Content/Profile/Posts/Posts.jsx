import React from "react";
import styles from './Posts.module.css'
import Post from "./Post/Post";
import {addPostActionCreator, updatePostTextActionCreator} from "../../../../redux/profileDataReducer";

function Posts(props) {
    let newPostElem = React.createRef();

    let addPost = () => {
        props.dispatch(addPostActionCreator());
    };

    let onPostChange = () => {
        let text = newPostElem.current.value;
        props.dispatch(updatePostTextActionCreator(text));
    };

    return(
        <div className={styles.posts}>
            <div className={styles.createPost}>
                <textarea className={styles.textArea}
                          ref={newPostElem}
                          placeholder="Create a post"
                          value={props.newPostText}
                          onChange={onPostChange} /><br/>

                <button className={styles.send} onClick={addPost}>Send</button>
            </div>

            <div className={styles.postList}>
                {props.state.map(x => {
                    return(
                        <Post avatar={x.userAvatar} name={x.fullName} content={x.postContent} />
                    );
                })}
            </div>
        </div>
    );
}

export default Posts;