import styles from './Post.module.css';

function Post(props) {
    return(
        <div className={styles.post}>
            <div className={styles.userImg}>
                <img src={props.avatar} alt=""/>
            </div>

            <div className={styles.postInfo}>
                <span className={styles.userName}>
                    {props.name}
                </span>

                <div className={styles.postContent}>
                    {props.content}
                </div>
            </div>
        </div>
    );
}

export default Post;