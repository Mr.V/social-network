import styles from './ProfileInfo.module.css';

function ProfileInfo(props) {
    return(
        <div className={styles.profileInfo}>
            <span className={styles.userName}>{props.state.fullName}</span>

            <div className={styles.data}>
                <span>Age: {props.state.age};</span><br/>
                <span>Education: {props.state.education};</span><br/>
                <span>Phone: {props.state.phone};</span><br/>
                <span>About me: {props.state.aboutSelf}</span>
            </div>
        </div>
    );
}

export default ProfileInfo;