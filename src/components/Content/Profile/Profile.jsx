import styles from './Profile.module.css';
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import Posts from "./Posts/Posts";

function Profile(props) {
    return(
        <div className={styles.profile}>
            <div className={styles.userAvatar}>
                <img src={props.state.profileInfo.userAvatar} alt=""/>
            </div>

            <ProfileInfo state={props.state.profileInfo} />

            <Posts state={props.state.posts}
                   newPostText={props.state.newPostText}
                   dispatch={props.dispatch}
            />
        </div>
    );
}

export default Profile;