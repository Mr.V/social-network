import styles from './Dialog.module.css';

function Dialog(props) {
    return (
        <div className={styles.dialog}>
            <div className={styles.userImg}>
                <img src={props.userAvatar} alt=""/>
            </div>

            <div className={styles.userInfo}>
                <span className={styles.userName}>
                    {props.userName}
                </span>

                <div className={styles.lastUserMessage}>
                    {props.lastUserMessage}
                </div>
            </div>
        </div>
    );
}

export default Dialog;