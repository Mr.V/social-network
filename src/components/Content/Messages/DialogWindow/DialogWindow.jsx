import React from "react";
import styles from './DialogWindow.module.css';
import {addMessageActionCreator, updateMessageTextActionCreator} from "../../../../redux/messagesDataReducer";

function DialogWindow(props) {
    let newMessage = React.createRef();

    let addMessage = () => {
        props.dispatch(addMessageActionCreator());
    };

    let onMessageChange = () => {
        let text = newMessage.current.value;
        props.dispatch(updateMessageTextActionCreator(text));
    };

    return (
        <div className={styles.dialogWindow}>
            <div className={styles.showMessages}>
                {props.messages.map(x => {
                    return <div className={styles.message}>{x.text}</div>
                })}
            </div>

            <div className={styles.inputMessage}>
                <textarea ref={newMessage} value={props.value} onChange={onMessageChange} placeholder='Input message'/>
                <button onClick={addMessage}>Send</button>
            </div>
        </div>
    );
}

export default DialogWindow;