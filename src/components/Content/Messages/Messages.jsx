import styles from './Messages.module.css';
import Dialog from "./Dialog/Dialog";
import {NavLink, Route} from "react-router-dom";
import DialogWindow from "./DialogWindow/DialogWindow";

function Messages(props) {
    return(
        <div className={styles.messages}>
            <div className={styles.dialogs}>
                {props.state.dialogs.map(x => {
                    return <NavLink to={x.linkTo}>
                                <Dialog userAvatar={x.userData.userAvatar}
                                        userName={x.userData.userName}
                                        lastUserMessage={x.lastUserMessage} />
                           </NavLink>;
                })}
            </div>

            <div className={styles.userMessages}>
                {/*<Route path={props.state.dialogWindow.path} render={() => <DialogWindow messages={props.state.dialogWindow.messages} dispatch={props.dispatch} />/>;*/}
                {/*{props.state.dialogWindows.map(x => {*/}
                {/*    return <Route path={x.path} render={() => <DialogWindow messages={x.messages} dispatch={props.dispatch} />} />;*/}
                {/*})}*/}
                <Route path={props.state.dialogWindow.path} render={() => <DialogWindow messages={props.state.dialogWindow.messages} value={props.state.dialogWindow.newMessageText} dispatch={props.dispatch} />} />
            </div>
        </div>
    );
}

export default Messages;