import Profile from "./Profile/Profile";
import Messages from "./Messages/Messages";
import {Route} from 'react-router-dom';

function Content(props) {
    return(
        <div className='content'>
            <Route path='/profile'  render={() => <Profile  state={props.state.profileData}  dispatch={props.dispatch} />}/>
            <Route path='/messages' render={() => <Messages state={props.state.messagesData} dispatch={props.dispatch} />}/>
        </div>
    );
}

export default Content;