import styles from './Header.module.css';

function Header() {
    return(
        <header className={styles.header}>
            <span>[LOGOTYPE]</span>
        </header>
    );
}

export default Header;