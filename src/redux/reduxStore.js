import {combineReducers, createStore} from "redux";
import profileDataReducer  from "./profileDataReducer";
import messagesDataReducer from "./messagesDataReducer";

let reducers = combineReducers({
    profileData:  profileDataReducer,
    messagesData: messagesDataReducer
});

let store = createStore(reducers);

export default store;