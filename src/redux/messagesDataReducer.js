import postAvatar from "../img/avatar-3637561_2.png";
let count = 0;

let initialState = {
    dialogs: [
        {id: 1, linkTo: '/messages/[UserName1]', userData: {userAvatar: postAvatar, userName: '[User Name1]'}, lastUserMessage: ''},
        {id: 2, linkTo: '/messages/[UserName2]', userData: {userAvatar: postAvatar, userName: '[User Name2]'}, lastUserMessage: ''},
        {id: 3, linkTo: '/messages/[UserName3]', userData: {userAvatar: postAvatar, userName: '[User Name3]'}, lastUserMessage: ''},
    ],

    dialogWindow: {path: '/messages/[UserName1]', messages: [], newMessageText: ''}
};

function messagesDataReducer(state = initialState, action) {
    switch(action.type) {
        case 'ADD-MESSAGE':
            let text = state.dialogWindow.newMessageText;
            state.dialogWindow.newMessageText = '';
            state.dialogWindow.messages.push({id: count+=1, text: text});
            return state;
        case 'UPDATE-MESSAGE-TXT':
            state.dialogWindow.newMessageText = action.newMessageText;
            return state;
        default: return state;
    }
}

export default messagesDataReducer;
export const   addMessageActionCreator        = ()     => {return { type: 'ADD-MESSAGE' }};
export const   updateMessageTextActionCreator = (text) => {return { type: 'UPDATE-MESSAGE-TXT', newMessageText: text }};