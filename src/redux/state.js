// import profileAvatar from '../img/avatar-3637561_1280.png';
// import postAvatar from '../img/avatar-3637561_2.png';
// import profileDataReducer from "./profileDataReducer";
// import messagesDataReducer from "./messagesDataReducer";
// const userName = '[User Name]';

// let store = {
//     _state: {
//         profileData: {
//             profileInfo: {
//                 userAvatar: profileAvatar,
//                 fullName: userName,
//                 age: 21,
//                 education: 'DAN.IT Education',
//                 phone: '+380 xx xxx xx xx',
//                 aboutSelf: 'This is something about me!'
//             },
//
//             posts: [],
//             newPostText: ''
//         },
//
//         messagesData: {
//             dialogs: [
//                 {id: 1, linkTo: '/messages/[UserName1]', userData: {userAvatar: postAvatar, userName: '[User Name1]'}, lastUserMessage: ''},
//                 {id: 2, linkTo: '/messages/[UserName2]', userData: {userAvatar: postAvatar, userName: '[User Name2]'}, lastUserMessage: ''},
//                 {id: 3, linkTo: '/messages/[UserName3]', userData: {userAvatar: postAvatar, userName: '[User Name3]'}, lastUserMessage: ''},
//             ],
//
//             dialogWindow: {path: '/messages/[UserName1]', messages: [], newMessageText: ''}
//
//             // dialogWindows: [
//             //     {id: 1, path: '/messages/[UserName1]', newMessageText: '', messages: []},
//             //     {id: 2, path: '/messages/[UserName2]', newMessageText: '', messages: []},
//             //     {id: 3, path: '/messages/[UserName3]', newMessageText: '', messages: []},
//             // ]
//         }
//     },
//
//     getState() {
//         return this._state
//     },
//
//     _callSubscriber() {
//         console.log('State is changed!');
//     },
//
//     subscribe(observer) {
//         this._callSubscriber = observer;
//     },
//
//     dispatch(action) {
//         profileDataReducer(this._state.profileData, action);
//         messagesDataReducer(this._state.messagesData, action);
//
//         this._callSubscriber(this._state);
//     }
// };

// export const addPostActionCreator        = ()     => {return { type: 'ADD-POST' }};
// export const updatePostTextActionCreator = (text) => {return { type: 'UPDATE-POST-TXT', newPostText: text }};
//
// export const addMessageActionCreator        = ()     => {return { type: 'ADD-MESSAGE' }};
// export const updateMessageTextActionCreator = (text) => {return { type: 'UPDATE-MESSAGE-TXT', newMessageText: text }};

// export default store;
// window.state = store;