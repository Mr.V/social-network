import postAvatar from "../img/avatar-3637561_2.png";
import profileAvatar from "../img/avatar-3637561_1280.png";
let count = 0;

let initialState = {
    profileInfo: {
        userAvatar: profileAvatar,
        fullName: '[User Name]',
        age: 21,
        education: 'DAN.IT Education',
        phone: '+380 xx xxx xx xx',
        aboutSelf: 'This is something about me!'
    },

    posts: [],
    newPostText: ''
};

function profileDataReducer(state = initialState, action) {
    switch (action.type) {
        case 'ADD-POST':
            const newPost = {
                id: count+=1,
                userAvatar: postAvatar,
                fullName: '[User Name]',
                postContent: state.newPostText
            };

            state.posts.unshift(newPost);
            state.newPostText = '';
            return state;
        case 'UPDATE-POST-TXT':
            state.newPostText = action.newPostText;
            return state;
        default: return state;
    }
}

export default profileDataReducer;
export const   addPostActionCreator        = ()     => {return { type: 'ADD-POST' }};
export const   updatePostTextActionCreator = (text) => {return { type: 'UPDATE-POST-TXT', newPostText: text }};