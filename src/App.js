import './App.css';
import Header from "./components/Header/Header";
import Navbar from "./components/Navbar/Navbar";
import Content from "./components/Content/Content";
import {BrowserRouter} from "react-router-dom";

function App(props) {
  return (
    <div className='app-wrapper'>
        <BrowserRouter>
            <Header />
            <Navbar />
            <Content state={props.state} dispatch={props.dispatch} />
        </BrowserRouter>
    </div>
  );
}

export default App;
